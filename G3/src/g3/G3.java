/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g3;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author Goldb
 */
public class G3 {
    
    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        int[][] matriz;
        
        matriz = new int[3][3];
        
        //for (int i = 0 ; i < 3 ; i++ ){
        //    for ( int z = 0 ; z < 3 ; z++ ){
        //        System.out.println("Introduza o valor da posição [" + (i+1) + "," + (z+1) + "]");
        //        matriz[i][z] = scanner.nextInt();
        //    }
        //}
        
        Scanner ler = new Scanner(new BufferedReader(new FileReader("matriz.txt")));
        while (ler.hasNextLine()){
            for (int i = 0 ; i < 3 ; i++ ) {
                String[] linha = ler.nextLine().trim().split(",");
                for ( int z = 0 ; z < linha.length ; z++ ) {
                    matriz[i][z] = Integer.parseInt(linha[z]);
                }
            }
        }
        
        String str;
        System.out.println("# | 0 1 2");
        for (int i = 0 ; i < 3 ; i++ ) {
            str = "";
            for ( int z = 0 ; z < 3 ; z++ ) {
                str = str + " " + matriz[i][z];
            }
            System.out.println(i + " |" + str);
        }
       
    }
    
    
}
