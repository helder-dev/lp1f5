/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g1;

import java.util.Scanner;

/**
 *
 * @author 1181539
 */
public class G1 {
    
    private static final Scanner scanner = new Scanner(System.in);
    
    public final static String FEIRA = "FEIRA";
    public final static String ISEP = "ISEP";
    public final static String IPP = "IPP";
    public final static String FEIRAISEP = "FEIRAISEP";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int count = 1, min, max;
        String str = "";
        
        System.out.print("Introduza o número inicial: ");
        min = scanner.nextInt();
        System.out.print("Introduza o número final: ");
        max = scanner.nextInt();
        
        while (Math.abs(min-max) < 100 ){
            System.out.println("A diferença entre os dois têm que ser maior que 100 . Introduza novamente");
            System.out.print("Introduza o número final: ");
            max = scanner.nextInt();
        }
        
        for (int i = min; i <= max; i++) {
            if (i % 3 == 0) {
                if (i % 5 == 0){
                    str = str + " " + FEIRAISEP;
                } else {
                    str = str + " " + FEIRA;
                }
            } else if (i % 5 == 0) {
                if (i % 3 == 0){
                    str = str + " " + FEIRAISEP;
                } else {
                    str = str + " " + IPP;
                }
            } else if (i % 7 == 0) {
                str = str + " " + ISEP;
            } else {
                str = str + " " + i;
            }
            
            
            if (count == 11) {
                System.out.println(str);
                str = "";
                count = 0;
            }
            
            count++;
        }
    }
    
}
