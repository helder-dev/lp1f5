/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g2;

import java.util.Scanner;

/**
 *
 * @author 1181539
 */
public class G2 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        double salario , salarioResto = 0.0, taxaEmpresa, taxaFuncionario;
        int idade;
        
        System.out.print("Introduza o salário do funcionário: ");
        salario = scanner.nextDouble();
        System.out.print("Introduza a idade do funcionário: ");
        idade = scanner.nextInt();
        
        if (salario > 6000){
            salarioResto = (salario - 6000);
            salario = 6000;
        }
        
        if (idade <= 55 ){
            taxaEmpresa = 0.20;
            taxaFuncionario = 0.17;
        } else if ( idade > 55 && idade <= 60 ){
            taxaEmpresa = 0.13;
            taxaFuncionario = 0.13;
        } else if ( idade > 60 && idade <= 65){
            taxaEmpresa = 0.075;
            taxaFuncionario = 0.09;
        } else {
            taxaEmpresa = 0.05;
            taxaFuncionario = 0.075;
        }
        
        double contribuicaoEmpresa = (taxaEmpresa * salario);
        double contribuicaoFuncionario = (taxaFuncionario * salario);
        
        System.out.println("Contribuição paga pela empresa: " + contribuicaoEmpresa);
        System.out.println("Contribuição paga pelo funcionário: " + contribuicaoFuncionario);
        
        System.out.println("Valor a receber pelo funcionário após descontos: " + ((salario - contribuicaoFuncionario) + salarioResto));
        
        
    }
    
}
